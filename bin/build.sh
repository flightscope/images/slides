#!/bin/bash

set -e
set -x

script_dir=$(dirname "$0")
dist_dir=${script_dir}/../public
dist_src_dir=${script_dir}/../.src
src_dir=${script_dir}/../src

mkdir -p $dist_dir

rsync -aH $src_dir/ $dist_src_dir

# crop Steve
if [ -f ${dist_src_dir}/steve-taddeucci.jpg ]; then
  mkdir -p ${dist_src_dir}
  convert -crop 2400x2400+900+0 +repage ${dist_src_dir}/steve-taddeucci.jpg ${dist_src_dir}/steve-taddeucci-cropped.jpg
fi

# crop Tony
if [ -f ${dist_src_dir}/tony-morris.jpg ]; then
  mkdir -p ${dist_src_dir}
  convert -crop 1600x1600+950+1400 +repage ${dist_src_dir}/tony-morris.jpg ${dist_src_dir}/tony-morris-cropped.jpg
fi

# crop Jimmy
if [ -f ${dist_src_dir}/jimmy-serrano.jpg ]; then
  mkdir -p ${dist_src_dir}
  convert -crop 2000x2000+1000+1100 +repage ${dist_src_dir}/jimmy-serrano.jpg ${dist_src_dir}/jimmy-serrano-cropped.jpg
fi

# crop ERSA wind component table
if [ -f ${dist_src_dir}/ersa_GEN-CON_wind-component_20200521.png ]; then
  mkdir -p ${dist_src_dir}
  convert -crop 1940x1605+140+150 +repage ${dist_src_dir}/ersa_GEN-CON_wind-component_20200521.png ${dist_src_dir}/ersa_GEN-CON_wind-component_20200521-cropped.png
fi

img_files=$(cd ${dist_src_dir} && find . -type f -name '*.png' -o -name '*.jpg' -o -name '*.webp')

IFS=$'\n'
for img_file in $img_files
do
  basename=$(basename -- "${img_file}")
  dirname=$(dirname -- "${img_file}")
  filename="${basename%.*}"
  extension="${basename##*.}"

  img=${dist_src_dir}/${img_file}

  mkdir -p ${dist_dir}/${dirname}
  img_prefix=${dist_dir}/${dirname}/${filename}

  for size in 1400 1024 800 450 250 100
  do
    convert ${img} -resize ${size}x${size} ${img_prefix}-${size}.${extension}
  done
done
IFS="$OIFS"

# rsync -aH $src_dir/ $dist_dir